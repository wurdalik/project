package ru.dalimo.akimov.mylearning.database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ru.dalimo.akimov.mylearning.Client;
import ru.dalimo.akimov.mylearning.Const;
import ru.dalimo.akimov.mylearning.Goods;
import ru.dalimo.akimov.mylearning.GoodsItem;
import ru.dalimo.akimov.mylearning.Manager;

public class ProjectLab {
    private static ProjectLab mProjectLab;
    private String userCode;
    private Context context;
    private SQLiteDatabase mDatabase;

    private static final String TAG = "ProjectLab";



    private ProjectLab(Context context){
        this.context = context;
        mDatabase = new ProjectBaseHelper(context).getWritableDatabase();
        userCode = Const.loadSettings(context).getCodeAgent();
    }

    public static ProjectLab get(Context context){
        if(mProjectLab==null){
            mProjectLab = new ProjectLab(context);
        }
        return mProjectLab;
    }

    /**
     * РАБОТА С КЛИЕНТАМИ
     */

    /** getClients();
     * Возвращает лист клиентов
     */
    public List<Client> getClients(){
        List<Client> clients = new ArrayList<>();

        ProjectCursorWrapper cursor = queryClients("userCode = ?", new String[]{userCode});
        try{
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                clients.add(cursor.getClient());
                cursor.moveToNext();
            }
        }
        finally {
            cursor.close();
        }
        return clients;
    }

    private ProjectCursorWrapper queryClients(String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(ProjectDbSchema.ClientsTable.NAME, null, whereClause, whereArgs, null, null, null);
        return new ProjectCursorWrapper(cursor);
    }

    /** addClient();
     * Добавление клиента в БД
     *
     */

      public void addClient(List<String[]>clients){
          long start = System.currentTimeMillis();
          ContentValues values = new ContentValues();
          mDatabase.beginTransaction();
          try{
              for(String[] client:clients){
                  values.put(ProjectDbSchema.ClientsTable.Cols.USER_CODE, client[0]);
                  values.put(ProjectDbSchema.ClientsTable.Cols.CLIENT_CODE, client[1]);
                  values.put(ProjectDbSchema.ClientsTable.Cols.TITLE, client[2]);
                  values.put(ProjectDbSchema.ClientsTable.Cols.INN, client[3]);
                  values.put(ProjectDbSchema.ClientsTable.Cols.ADDRESS, client[4]);
                  values.put(ProjectDbSchema.ClientsTable.Cols.NUMBER, client[5]);
                  values.put(ProjectDbSchema.ClientsTable.Cols.DIRECTOR, client[6]);

                  Long d = mDatabase.insert(ProjectDbSchema.ClientsTable.NAME, null, values);
                  Log.i(TAG, "addClient: "+ d);
              }
             mDatabase.setTransactionSuccessful();}
          catch (Exception e){
              e.printStackTrace();
          }
          finally {
              mDatabase.endTransaction();
          }
          long finish = System.currentTimeMillis();
          Log.i(TAG, String.format("Clients -> %d", finish-start));
      }

    private static ContentValues getContentValues(Client client){
        ContentValues values = new ContentValues();
        values.put(ProjectDbSchema.ClientsTable.Cols.CLIENT_CODE, client.getClientCode());
        values.put(ProjectDbSchema.ClientsTable.Cols.TITLE, client.getClientTitle());
        values.put(ProjectDbSchema.ClientsTable.Cols.INN, client.getInn());
        values.put(ProjectDbSchema.ClientsTable.Cols.ADDRESS, client.getClientAddress());
        values.put(ProjectDbSchema.ClientsTable.Cols.NUMBER, client.getClientNumber());
        values.put(ProjectDbSchema.ClientsTable.Cols.DIRECTOR, client.getClientDirector());
        return values;
    }

    /**
     * ///////////////////////////////////////////////////////////////////
     */

    /**
     * РАБОТА С МЕНЕДЖЕРАМИ
     */

    public List<String> getManagers(String clientCode){
        List<String> managers = new ArrayList<>();
        ProjectCursorWrapper cursor = queryManagers("clientCode = ?", new String[]{clientCode});
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                managers.add(cursor.getManager(userCode, clientCode).getNameManag());
                cursor.moveToNext();
            }
        }
        finally {
            cursor.close();
        }
        return managers;
    }

    private ProjectCursorWrapper queryManagers(String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(ProjectDbSchema.ManagersTable.NAME, null, whereClause, whereArgs, null, null, null);
        return new ProjectCursorWrapper(cursor);
    }

    public void addManager(List<String[]>managers){
        Long start = System.currentTimeMillis();
        mDatabase.beginTransaction();
        ContentValues values = new ContentValues();
        try{
            for(String[]manager:managers){
                values.put(ProjectDbSchema.ManagersTable.Cols.USER_CODE, manager[0]);
                values.put(ProjectDbSchema.ManagersTable.Cols.CLIENT_CODE, manager[1]);
                values.put(ProjectDbSchema.ManagersTable.Cols.MANAGER_CODE, manager[2]);
                values.put(ProjectDbSchema.ManagersTable.Cols.MANAGER_NAME, manager[3]);

                Long d = mDatabase.insert(ProjectDbSchema.ManagersTable.NAME, null, values);
                Log.i(TAG, "addManager: " + d);
            }
            mDatabase.setTransactionSuccessful();
        }
        catch (Exception e){
            Log.d(TAG, e.getMessage() );
        }
        finally {
            mDatabase.endTransaction();
        }
        Long finish = System.currentTimeMillis();
        Log.i(TAG, String.format("Managers -> %d ", finish-start));
      }

    private static ContentValues getContentValues(String codeClient, Manager manager){
        ContentValues values = new ContentValues();
        values.put(ProjectDbSchema.ManagersTable.Cols.CLIENT_CODE, codeClient);
        values.put(ProjectDbSchema.ManagersTable.Cols.MANAGER_CODE, manager.getCodeManag());
        values.put(ProjectDbSchema.ManagersTable.Cols.MANAGER_NAME, manager.getNameManag());
        return values;
    }

    /**
     * ////////////////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * РАБОТА С МАТРИЦЕЙ
     */

      public void writeMatrix(List<String[]>matrix){
          Long start = System.currentTimeMillis();
          mDatabase.beginTransaction();
          ContentValues values = new ContentValues();
          try {
              for(String[]good:matrix){
                  values.put(ProjectDbSchema.Matrix.Cols.USER_CODE, good[0]);
                  values.put(ProjectDbSchema.Matrix.Cols.CLIENT_CODE, good[1]);
                  values.put(ProjectDbSchema.Matrix.Cols.PRODUCT_CODE, good[2]);
                  values.put(ProjectDbSchema.Matrix.Cols.EXCLUSIVE, good[3]);

                  Long d = mDatabase.insert(ProjectDbSchema.Matrix.NAME, null, values);
                  Log.i(TAG, "writeMatrix: "+ d);
              }
              mDatabase.setTransactionSuccessful();
          }catch (Exception e){
              e.printStackTrace();
          }
          finally {
              mDatabase.endTransaction();
          }
          Long finish = System.currentTimeMillis();
          Log.i(TAG, String.format("Matrix -> %d", finish-start));
      }

      public void writeGoods(List<String[]>goods){
          Long start = System.currentTimeMillis();
          mDatabase.beginTransaction();
          ContentValues values = new ContentValues();
         try{
             for(String[]good:goods){
                 values.put(ProjectDbSchema.GoodsMetaData.Cols.PRODUCT_TYPE, good[0]);
                 values.put(ProjectDbSchema.GoodsMetaData.Cols.PARENT_CODE, good[1]);
                 values.put(ProjectDbSchema.GoodsMetaData.Cols.PRODUCT_CODE, good[2]);
                 values.put(ProjectDbSchema.GoodsMetaData.Cols.PRODUCT_NAME, good[3]);

                 Long d = mDatabase.insert(ProjectDbSchema.GoodsMetaData.NAME, null, values);
                 Log.i(TAG, "writeGoods: " + d);
             }
             mDatabase.setTransactionSuccessful();
         }
         catch (Exception e){
             Log.e(TAG, "writeGoods: " + e.getMessage() );
         }
         finally {
             mDatabase.endTransaction();
         }

          Long finish = System.currentTimeMillis();
          Log.i(TAG, String.format("Goods -> %d:", finish-start));
      }

      public ArrayList<GoodsItem> getGoods(){
          List<Goods>goods = new ArrayList<>();
          ArrayList<GoodsItem>goodsItem = new ArrayList<>();
          ProjectCursorWrapper cursor = queryGoods("productType = ?", new String[]{"1"});
            try{
                cursor.moveToFirst();
                while(!cursor.isAfterLast()){
                    Goods g = cursor.getGoods();
                    if(g.getParentCode().equalsIgnoreCase("00000")){
                        goodsItem.add(new GoodsItem(g, 0, true) );
                    }
                    else{
                        goodsItem.add(new GoodsItem(g, 1, false));
                    }
                    cursor.moveToNext();
                }
            }
            catch (Exception e ) {
            Const.Toast((Activity) context, e.getMessage());
            return null;
            }
        return goodsItem;
      }

//      public ProjectCursorWrapper query(){
//
//      }

      public ProjectCursorWrapper queryGoods(String whereClause, String [] whereArgs){
        Cursor cursor = mDatabase.query(ProjectDbSchema.GoodsMetaData.NAME, null, whereClause, whereArgs, null, null, null);
        return new ProjectCursorWrapper(cursor);
      }

}
