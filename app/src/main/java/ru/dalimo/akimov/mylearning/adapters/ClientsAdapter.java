package ru.dalimo.akimov.mylearning.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.dalimo.akimov.mylearning.Client;
import ru.dalimo.akimov.mylearning.R;

public class ClientsAdapter extends ArrayAdapter<Client> implements Filterable {
    private Filter itemFilter;

    private List<Client> clients;
    LayoutInflater inflater;
    private int resource;
    private Activity context;

    public ClientsAdapter(Activity context, int resource, List<Client> clients) {
        super(context, resource, clients);
        this.context = context;
        this.resource = resource;
        this.inflater = LayoutInflater.from(context);
        this.clients = clients;
    }

    @Override
    public int getCount() {
        return clients!=null?clients.size():0;
    }

    @Override
    public Client getItem(int position) {
        return clients.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;
        if(convertView == null){
            view = inflater.inflate(R.layout.client_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.clientTitle = view.findViewById(R.id.clientTitle);
            viewHolder.clientAddress = view.findViewById(R.id.clientAddress);
            view.setTag(viewHolder);
        } else{
            viewHolder = (ViewHolder)view.getTag();
        }
        Client client = clients.get(position);

        viewHolder.clientTitle.setText(client.getClientTitle());
        viewHolder.clientAddress.setText(client.getClientAddress());
        return view;
    }

    private class ViewHolder{
        TextView clientTitle;
        TextView clientAddress;
    }

    @Override
    public Filter getFilter() {
        if(itemFilter == null){
            itemFilter = new ItemFilter();
        }
        return itemFilter;
    }


    private class ItemFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();

            if(constraint!=null||constraint.length()>0){
                List clients_filter = new ArrayList<Client>();

                for(int i = 0; i <= clients.size()-1; i++){
                    if(clients.get(i).getClientTitle().toLowerCase().contains(constraint.toString().toLowerCase())){
                        clients_filter.add(clients.get(i));
                    }
                }
                filterResults.values = clients_filter;
                filterResults.count = clients_filter.size();
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clients = (List<Client>) results.values;
            ClientsAdapter.this.notifyDataSetChanged();}

    }


}