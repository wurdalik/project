package ru.dalimo.akimov.mylearning.asynctasks;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.ProgressBar;

import ru.dalimo.akimov.mylearning.Const;
import ru.dalimo.akimov.mylearning.ifaces.MyLocationListener;
import ru.dalimo.akimov.mylearning.ifaces.IGetLocationComplete;

public class GetLocationTask extends AsyncTask<Void, Integer, Boolean>{
    private ProgressBar progressBar;
    boolean running = true;
    Context context;
    private MyLocationListener gps;
    private MyLocationListener network;
    private LocationManager locationManager;
    private Location result;
    private int second = 0;

    private IGetLocationComplete listener;


    public static final String TAG = "GetLocationTask";

    public GetLocationTask(Context context, ProgressBar progressBar, IGetLocationComplete listener){
        this.context = context;
        gps = new MyLocationListener();
        network = new MyLocationListener();
        this.progressBar = progressBar;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar.setIndeterminate(true);
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        requestLocationUpdates(gps, LocationManager.GPS_PROVIDER, 300 );
        requestLocationUpdates(network, LocationManager.NETWORK_PROVIDER, 300 );
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        while((result==null || result.getAccuracy()>300)&& second<= 30){
//            publishProgress(second);
            result = network.lastLocation();
            if(isCancelled()){
                break;
            }
            if(result == null){
                result = gps!=null ? gps.lastLocation() : null;
            } else{
                Location last = gps!=null ? gps.lastLocation():null;
                if(last!=null&&result.getAccuracy()>last.getAccuracy()){
                    result = last;
                }
            }
            try{
                Thread.sleep(1000);
            } catch (Exception e) {
                Log.e(TAG, "doInBackground: " + e.getMessage() );
            }
            second++;
        }
        if(second == 30){
            if(gps.lastLocation()!=null){
                result = gps.lastLocation();
            }
            else if(network.lastLocation()!=null){
                result = network.lastLocation();
            }
            else{
                Const.Toast((Activity) context, "Не удалось определить местоположение");
            }
        }

        return result!=null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        progressBar.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        listener.onGetLocationComplete(this, result);
    }

    private void requestLocationUpdates(MyLocationListener listener, String provider, float minDistance){
        try{
            if(locationManager == null){
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        }
        if(ActivityCompat.checkSelfPermission(context,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            return;
        }
        locationManager.requestLocationUpdates(provider, 0, minDistance, listener); }
        catch(Exception e){
            Log.e(TAG, "requestLocationUpdates: " + e.getMessage());
    } 
        
    }

    private void parceManage(){

    }

}
