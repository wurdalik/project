package ru.dalimo.akimov.mylearning.asynctasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import java.io.File;

import ru.dalimo.akimov.mylearning.Const;
import ru.dalimo.akimov.mylearning.ifaces.IGetFilesComplete;
import ru.dalimo.akimov.mylearning.connections.UserSettings;
import ru.dalimo.akimov.mylearning.connections.MMerchClient;

public class GetFilesFromServerTask extends AsyncTask<String, Integer, Boolean> {
    private Activity context;
    private UserSettings userSettings;
    private IGetFilesComplete mTaskCompleteListener;
    private ProgressDialog progress;
    int all = 0;
    private String mResultString;

    private MMerchClient mClient;

    public GetFilesFromServerTask(Activity context, UserSettings mSettings, IGetFilesComplete mTaskCompleteListener){
        this.context = context;
        this.mTaskCompleteListener = mTaskCompleteListener;
        this.userSettings = mSettings;
        progress = new ProgressDialog(context);
        progress.setIndeterminate(true);
        progress.setMessage("Загрузка файла");
        progress.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progress.show();
        mClient = new MMerchClient(context, userSettings);
    }

    @Override
    protected Boolean doInBackground(String... files) {
        if(userSettings.getCodeAgent()==null|| userSettings.getCodeAgent()==""){
            Const.Toast(context, "Укажите код менеджера в настройках");
            return false;
        }

        mResultString = "";
        for(String item : files){
            if(!mClient.connect()){
                mResultString = "Не удалось подключиться";
                return false;
            }

            String message = String.format("%s;%s;%s", item, userSettings.getCodeAgent(), Const.END_MESSAGE);
            mClient.send(message);

            if(!mClient.receiveFile(Const.prepareWorkPath() + File.separator + item + Const.ARCH_EXT)){
                mResultString += mClient.LastError() + Const.NEWLINE;
                mClient.disconnect();
                return false;
            }
            else{
                publishProgress(files.length);
                mResultString += "Success" + Const.NEWLINE;
            }

            mClient.disconnect();
        }
        return true;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        progress.setIndeterminate(false);
        progress.setMax(values.length);
        progress.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if(mTaskCompleteListener!=null){
            mTaskCompleteListener.onTaskComplete(this);
        }
        progress.dismiss();
    }
}
