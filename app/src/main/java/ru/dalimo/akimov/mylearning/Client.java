package ru.dalimo.akimov.mylearning;

import android.os.Parcel;
import android.os.Parcelable;

public class Client implements Parcelable {
    public static final String KEY = "client";

    private String clientCode;
    private String clientTitle;
    private String inn;
    private String clientAddress;
    private String clientNumber;
    private String clientDirector;

    public Client(String clientCode){
        this(clientCode, "","","","","");
    }

    public Client(String clientCode, String clientTitle, String inn, String clientAddress, String clientNumber, String clientDirector){
        this.clientCode = clientCode;
        this.clientTitle = clientTitle;
        this.inn = inn;
        this.clientAddress = clientAddress;
        this.clientNumber = clientNumber;
        this.clientDirector = clientDirector;
    }

    public Client(){
        this("","","","","","");
    }


    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientTitle() {
        return clientTitle;
    }

    public void setClientTitle(String clientTitle) {
        this.clientTitle = clientTitle;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getClientDirector() {
        return clientDirector;
    }

    public void setClientDirector(String clientDirector) {
        this.clientDirector = clientDirector;
    }


    public static Creator<Client> CREATOR = new Creator<Client>() {
        @Override
        public Client createFromParcel(Parcel source) {
            return new Client(source);
        }

        @Override
        public Client[] newArray(int size) {
            return new Client[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    protected Client(Parcel in){
        clientCode = in.readString();
        clientTitle = in.readString();
        inn = in.readString();
        clientAddress = in.readString();
        clientNumber = in.readString();
        clientDirector = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(clientCode);
        dest.writeString(clientTitle);
        dest.writeString(inn);
        dest.writeString(clientAddress);
        dest.writeString(clientNumber);
        dest.writeString(clientDirector);
    }
}


