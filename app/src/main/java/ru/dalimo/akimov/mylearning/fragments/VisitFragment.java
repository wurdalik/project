package ru.dalimo.akimov.mylearning.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;


import ru.dalimo.akimov.mylearning.Client;
import ru.dalimo.akimov.mylearning.R;
import ru.dalimo.akimov.mylearning.asynctasks.GetLocationTask;
import ru.dalimo.akimov.mylearning.database.ProjectLab;








public class VisitFragment extends Fragment  implements IGetLocationComplete, View.OnClickListener {
    private ProgressBar progressBar;
    private Button createMonitoring;
    private Button createTask;
    private Button updateTask;
    private Button sendPhoto;


    private GetLocationTask getLocationTask;
    private Context context;
    private Client client;


    private MonitoringFragment monitoringFragment;

    private List<String>managers;

    public static VisitFragment newInstance(Client client){
        VisitFragment visitFragment = new VisitFragment();
        Bundle args = new Bundle();
        args.putParcelable(Client.KEY, client);
        visitFragment.setArguments(args);
        return visitFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = getArguments().getParcelable(Client.KEY);
        managers = ProjectLab.get(getActivity()).getManagers(client.getClientCode());
        context = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.visit_layout,  null);
        createMonitoring = v.findViewById(R.id.create_monitoring);
        createTask = v.findViewById(R.id.create_task);
        updateTask = v.findViewById(R.id.update_tasks);
        sendPhoto = v.findViewById(R.id.send_photo);

        createMonitoring.setOnClickListener(this);
        createTask.setOnClickListener(this);
        updateTask.setOnClickListener(this);
        sendPhoto.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.create_monitoring:
                monitoringFragment = MonitoringFragment.newInstance(client);
                FragmentManager ft = getActivity().getSupportFragmentManager();
                                ft.beginTransaction()
                                .replace(R.id.fragment_container, monitoringFragment)
                                .addToBackStack(null)
                                .commit();
                break;
            case R.id.create_task:

                break;
            case R.id.update_tasks:

                break;
            case R.id.send_photo:

                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.create_monitoring:
                MonitoringFragment fragment = MonitoringFragment.newInstance(client);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
        }
    }

}
