package ru.dalimo.akimov.mylearning.fragments;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.util.ArrayList;
import java.util.List;

import ru.dalimo.akimov.mylearning.Client;
import ru.dalimo.akimov.mylearning.Const;
import ru.dalimo.akimov.mylearning.R;
import ru.dalimo.akimov.mylearning.adapters.ClientsAdapter;
import ru.dalimo.akimov.mylearning.database.ProjectLab;

public class ListClientsFragment extends Fragment implements MenuItemCompat.OnActionExpandListener, SearchView.OnQueryTextListener {
    private ListView mClientList;
    private ClientsAdapter clientsAdapter;
    private List<Client> clients;
    private Toolbar toolbar;
    private SearchView searchView;
    private VisitFragment visitFragment;
    private MenuItem searchMenuItem;
    private String userCode;


    public static ListClientsFragment newInstatce(String userCode){
        Bundle args = new Bundle();
        ListClientsFragment listClientsFragment = new ListClientsFragment();
        args.putString("userCode", userCode);
        listClientsFragment.setArguments(args);
        return listClientsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userCode = getArguments().getString("userCode");
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.monitoring, null);
        mClientList = v.findViewById(R.id.client_list);
        clients = ProjectLab.get(getActivity()).getClients();
        clientsAdapter = new ClientsAdapter(getActivity(), R.id.client_list, clients);
        mClientList.setAdapter(clientsAdapter);
        mClientList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            Permissions.check(getActivity(), Const.PERMISSIONS, Const.rationale, Const.options, new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        Client client = clients.get(position);
                        visitFragment = VisitFragment.newInstance(client);
                        FragmentTransaction fm = getActivity().getSupportFragmentManager().beginTransaction();
                        fm.replace(R.id.fragment_container, visitFragment)
                                .addToBackStack(null)
                                .commit();
                    }

                    @Override
                    public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                        Const.Toast(getActivity(), "В доступе отказано");
                    }
                });




            }
        });
        clientsAdapter.notifyDataSetChanged();
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.monitoring_menu, menu);
        searchMenuItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(searchMenuItem, this);
    }


    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {

        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        clients = ProjectLab.get(getActivity()).getClients();
        clientsAdapter = new ClientsAdapter(getActivity(), R.id.client_list, clients);
        mClientList.setAdapter(clientsAdapter);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        clientsAdapter.getFilter().filter(s);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }


}
