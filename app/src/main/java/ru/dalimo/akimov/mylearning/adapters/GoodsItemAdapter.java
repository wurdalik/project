package ru.dalimo.akimov.mylearning.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ru.dalimo.akimov.mylearning.GoodsItem;
import ru.dalimo.akimov.mylearning.R;

public class GoodsItemAdapter extends RecyclerView.Adapter<GoodsItemHolder>{
    private ArrayList<GoodsItem> goodsItem;
    private Context context;

    public GoodsItemAdapter(Context context, ArrayList<GoodsItem>goodsItem){
        this.context = context;
        this.goodsItem = goodsItem;
int a = 35+64;
    }

    @NonNull
    @Override
    public GoodsItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i){
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.goods_item_layout, viewGroup, false);
        return new GoodsItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GoodsItemHolder goodsItemHolder, int i){
        GoodsItem goods = goodsItem.get(i);
        goodsItemHolder.bindGoodsItem(goods);
    }

    @Override
    public int getItemCount() {
        return goodsItem.size();
    }
}