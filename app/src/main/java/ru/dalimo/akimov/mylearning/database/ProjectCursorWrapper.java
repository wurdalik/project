package ru.dalimo.akimov.mylearning.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import ru.dalimo.akimov.mylearning.Client;
import ru.dalimo.akimov.mylearning.Const;
import ru.dalimo.akimov.mylearning.Goods;
import ru.dalimo.akimov.mylearning.GoodsItem;
import ru.dalimo.akimov.mylearning.Manager;

public class ProjectCursorWrapper extends CursorWrapper {

    public ProjectCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Client getClient(){
        String clientCode = getString(getColumnIndex(ProjectDbSchema.ClientsTable.Cols.CLIENT_CODE));
        String clientTitle = getString(getColumnIndex(ProjectDbSchema.ClientsTable.Cols.TITLE));
        String inn = getString(getColumnIndex(ProjectDbSchema.ClientsTable.Cols.INN));
        String clientAddress = getString(getColumnIndex(ProjectDbSchema.ClientsTable.Cols.ADDRESS));
        String clientNumber = getString(getColumnIndex(ProjectDbSchema.ClientsTable.Cols.NUMBER));
        String clientDirector = getString(getColumnIndex(ProjectDbSchema.ClientsTable.Cols.DIRECTOR));

        Client client = new Client(clientCode);
        client.setClientTitle(clientTitle);
        client.setInn(inn);
        client.setClientAddress(clientAddress);
        client.setClientNumber(clientNumber);
        client.setClientDirector(clientDirector);
        return client;
    }

    public Manager getManager(String code, String client){
        String userCode = getString(getColumnIndex(ProjectDbSchema.ManagersTable.Cols.CLIENT_CODE));
        String codeClient = getString(getColumnIndex(ProjectDbSchema.ManagersTable.Cols.CLIENT_CODE));
        String codeManag = getString(getColumnIndex(ProjectDbSchema.ManagersTable.Cols.MANAGER_CODE));
        String nameManag = getString(getColumnIndex(ProjectDbSchema.ManagersTable.Cols.MANAGER_NAME));

        Manager manager = new Manager(codeManag, nameManag);
        return manager;
    }

    public Goods getGoods(){
        String goodType = getString(getColumnIndex(ProjectDbSchema.GoodsMetaData.Cols.PRODUCT_TYPE));
        String parentCode = getString(getColumnIndex(ProjectDbSchema.GoodsMetaData.Cols.PARENT_CODE));
        String goodCode = getString(getColumnIndex(ProjectDbSchema.GoodsMetaData.Cols.PRODUCT_CODE));
        String productName = getString(getColumnIndex(ProjectDbSchema.GoodsMetaData.Cols.PRODUCT_NAME));

        Goods goods = new Goods(goodType, parentCode, goodCode, productName);
        return goods;
    }

}
