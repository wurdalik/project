package ru.dalimo.akimov.mylearning.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.dalimo.akimov.mylearning.GoodsItem;
import ru.dalimo.akimov.mylearning.R;

public class GoodsItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView  mTitle;
    float dp = 4f;

    public void bindGoodsItem(GoodsItem goodsItem){
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)itemView.getLayoutParams();
        mTitle.setText(goodsItem.getGoods().getProductName());
//        if(!goodsItem.isVisible()){
//            itemView.setVisibility(View.GONE);
//        }
//        else{
//            itemView.setVisibility(View.VISIBLE);
//        }
        if(goodsItem.getLevel()>0){
            params.leftMargin = (goodsItem.getLevel())+4;
        }
        itemView.setLayoutParams(params);
    }

    public GoodsItemHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        mTitle = itemView.findViewById(R.id.goods_item_title);
    }

    @Override
    public void onClick(View v) {

    }
}
