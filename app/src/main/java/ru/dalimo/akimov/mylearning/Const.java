package ru.dalimo.akimov.mylearning;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Xml;
import android.widget.Toast;

import com.nabinbhandari.android.permissions.Permissions;

import java.io.File;

import ru.dalimo.akimov.mylearning.connections.UserSettings;

public class Const {
    public static final String WIN1251 = "windows-1251";
    public static final String UTF8 = Xml.Encoding.UTF_8.name();
    public static final String END_MESSAGE = "<EOF>";
    public static final String NEWLINE = "\n";

    public static final String PATH = Environment.getExternalStorageDirectory() +  File.separator + "MMerch";
    public static final String END_FOLDER = "FILES";
    public static final String ARCH_EXT = ".zip";
    public static final String ARCHIVE_NAME = "FILES_MONITORING";


    public static final String USER_CODE = "userCode";
    public static final String SERVER = "server";
    public static final String PORT = "port";


    public static final String KLIENTS = "KLIENTS_M.TXT";
    public static final String MANAGERS = "MANAG_M.TXT";
    public static final String MATRIX = "MATRIX_M.TXT";
    public static final String OSTATKI = "OST_SKL_M.TXT";
    public static final String SMTP = "SMTP.TXT";

    public static final String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    public final static String rationale = "Для обновления файлов, программе необходим доступ ко внутреннему накопителю";
    public final static Permissions.Options options = new Permissions.Options().setRationaleDialogTitle("Информация");

    public static String prepareWorkPath(){
        File f = new File(PATH);
        if(!f.exists()){
            f.mkdirs();
        }
        return f.getAbsolutePath();
    }

    public static void Toast(final Activity context, final String message){
        context.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void Toast(final Activity context, final int resId){
        context.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static UserSettings loadSettings(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String userCode = sp.getString(USER_CODE, null);
        String server = sp.getString(SERVER, null);
        int port = Integer.valueOf(sp.getString(PORT, "11000"));
        UserSettings userSettings = new UserSettings(userCode, server, port);
        return userSettings;
    }
}
