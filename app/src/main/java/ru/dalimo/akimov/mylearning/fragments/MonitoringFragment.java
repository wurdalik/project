package ru.dalimo.akimov.mylearning.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import ru.dalimo.akimov.mylearning.Client;
import ru.dalimo.akimov.mylearning.Goods;
import ru.dalimo.akimov.mylearning.GoodsItem;
import ru.dalimo.akimov.mylearning.R;
import ru.dalimo.akimov.mylearning.asynctasks.GetLocationTask;
import ru.dalimo.akimov.mylearning.database.ProjectLab;

public class MonitoringFragment extends Fragment implements View.OnClickListener {
    private Button setDate;
    private Button addGoods;
    private Spinner spinner;
    private Calendar calendar;
    private DateFormat dateFormat;
    private Context context;
    private Client client;




    private List<String> managers;
    private ArrayList<GoodsItem>goods;

    public static MonitoringFragment newInstance(Client client){
        Bundle args = new Bundle();
        args.putParcelable(Client.KEY, client);
        MonitoringFragment monitoringFragment = new MonitoringFragment();
        monitoringFragment.setArguments(args);
        return monitoringFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        context = getActivity();
        client = getArguments().getParcelable(Client.KEY);
        managers = ProjectLab.get(getActivity()).getManagers(client.getClientCode());
        goods = ProjectLab.get(getActivity()).getGoods();
        calendar = Calendar.getInstance();
        dateFormat = DateFormat.getDateInstance();
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.monitoring_layout, null);
        spinner = v.findViewById(R.id.managers_spin);
        spinner.setAdapter(new ArrayAdapter<>(context, R.layout.spinner_layout, managers));
        setDate = v.findViewById(R.id.setDate);
        setDate.setOnClickListener(this);
        setDate.setText(dateFormat.format(calendar.getTime()));
        addGoods = v.findViewById(R.id.add_goods);
        addGoods.setOnClickListener(this);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.setDate:
                calendar = Calendar.getInstance();

                DatePickerDialog dialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar = Calendar.getInstance();
                        dateFormat = DateFormat.getDateInstance();
                        calendar.set(year, month, dayOfMonth);
                        setDate.setText(dateFormat.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
                break;
            case R.id.add_goods:
                ListGoodsFragment listGoodsFragment = ListGoodsFragment.newInstance(goods);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction()
                        .replace(R.id.fragment_container, listGoodsFragment)
                        .addToBackStack(null)
                        .commit();
        }
    }

}