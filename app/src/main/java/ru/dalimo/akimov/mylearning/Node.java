package ru.dalimo.akimov.mylearning;

import android.os.Parcel;
import android.os.Parcelable;

public class Node implements Parcelable {
    public static final String KEY = "GoodsGroupNode";
    public static final String NODE_KEY = "node";
    public static final String SUBNODE_KEY = "subnode";

    private String mParentCode;
    private String mCodeGroup;
    private String mNameGroup;
    private int mChildCount;

    private boolean mIsExpanded;

    private Node(Parcel data){
        mParentCode = data.readString();
        mCodeGroup = data.readString();
        mNameGroup = data.readString();
        mChildCount = data.readInt();

        boolean[] val = new boolean[1];
        data.readBooleanArray(val);
        mIsExpanded = val[0];
    }

    public Node(String code, String parent, String name){
        mParentCode = parent;
        mCodeGroup = code;
        mNameGroup = name;
        mIsExpanded = false;
        mChildCount = 0;
    }

    public String getName(){
        return mNameGroup;
    }

    public void setName(String name){
        mNameGroup = name;
    }

    public String getParentCode(){
        return mParentCode;
    }

    public void setParentCode(String parentCode){
        mParentCode = parentCode;
    }

    public String getCode(){
        return mCodeGroup;
    }

    public void setCode(String code){
        mCodeGroup = code;
    }

    public boolean isExpanded(){
        return mIsExpanded;
    }

    public void setExpanded(boolean value){
        mIsExpanded = value;
    }

    public void setChildCount(int value){
        mChildCount = value;
    }

    public int getChildCount(){
        return mChildCount;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel data, int j) {
        data.writeString(mParentCode);
        data.writeString(mCodeGroup);
        data.writeString(mNameGroup);
        data.writeInt(mChildCount);

        data.writeBooleanArray(new boolean[]{mIsExpanded});
    }

    public static final Parcelable.Creator<Node> CREATOR = new Parcelable.Creator<Node>() {

        public Node createFromParcel(Parcel in) {
            return new Node(in);
        }

        public Node[] newArray(int size) {
            return new Node[size];
        }
    };

}
