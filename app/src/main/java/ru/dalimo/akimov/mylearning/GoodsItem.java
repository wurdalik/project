package ru.dalimo.akimov.mylearning;

import android.os.Parcel;
import android.os.Parcelable;

public class GoodsItem implements Parcelable {
    private int level;
    private boolean isVisible;
    private Goods goods;

    public GoodsItem(Goods goods, int level, boolean isVisible){
        this.goods = goods;
        this.isVisible = isVisible;
        this.level = level;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(goods, flags);
        dest.writeByte((byte) (isVisible?1:0));
        dest.writeInt(level);
    }

    public static Parcelable.Creator<GoodsItem> CREATOR = new Parcelable.Creator<GoodsItem>(){

        @Override
        public GoodsItem createFromParcel(Parcel source) {
            return new GoodsItem(source);
        }

        @Override
        public GoodsItem[] newArray(int size) {
            return new GoodsItem[size];
        }
    };

    public GoodsItem(Parcel in){
        goods = in.readParcelable(Goods.class.getClassLoader());
        isVisible = in.readByte()!=0;
        level = in.readInt();
    }
}
