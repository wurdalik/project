package ru.dalimo.akimov.mylearning.ifaces;

public interface ISettingsChangeListener {
    void onSettingsChange();
}
