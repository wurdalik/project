package ru.dalimo.akimov.mylearning.connections;

import android.os.Parcel;
import android.os.Parcelable;

public class UserSettings implements Parcelable {
    public static final String KEY = "user_settings";
    private String codeAgent;
    private String server;
    private int port;

    public UserSettings(String codeAgent, String server, int port){
        this.codeAgent = codeAgent;
        this.server = server;
        this.port = port;
    }

    protected UserSettings(Parcel in) {
        codeAgent = in.readString();
        server = in.readString();
        port = in.readInt();
    }

    public static final Creator<UserSettings> CREATOR = new Creator<UserSettings>() {
        @Override
        public UserSettings createFromParcel(Parcel in) {
            return new UserSettings(in);
        }

        @Override
        public UserSettings[] newArray(int size) {
            return new UserSettings[size];
        }
    };

    public String getCodeAgent() {
        return codeAgent;
    }

    public void setCodeAgent(String codeAgent) {
        this.codeAgent = codeAgent;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(codeAgent);
        dest.writeString(server);
        dest.writeInt(port);
    }
}
