package ru.dalimo.akimov.mylearning.connections;

import android.app.Activity;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

import ru.dalimo.akimov.mylearning.Const;

public class MMerchClient {
    public static final String ERROR = "ERR";
    private String server;
    private int port;

    Activity context;
    Socket mSocket;

    InputStream in = null;
    OutputStream out = null;

    private final short timeout = 30;

    private String mLastError;

    public MMerchClient(Activity context, UserSettings settings){
        this.context = context;
        this.server = settings.getServer();
        this.port = settings.getPort();
    }

    private boolean createSocket(InetAddress inetAddress, int port){
        try {
            mSocket = new Socket();
            InetSocketAddress socketAddress = new InetSocketAddress(inetAddress, port);
            mSocket.connect(socketAddress, timeout*1000);
        } catch (IOException e) {
            mLastError = e.toString();
            Const.Toast(context, mLastError);
            return false;
        }
        return true;
    }

    public boolean connect(){
        try {
                InetAddress mServer = InetAddress.getByName(server);
                if(!createSocket(mServer, port)){
                    Const.Toast(context, "Не удалось подключиться");
            }

            if(mSocket.isConnected()){
            in = mSocket.getInputStream();
            out = mSocket.getOutputStream();}

            else{
                mLastError = "Не удалось подключиться к серверу";
                Const.Toast(context, mLastError);
                return false;
            }

        } catch (IOException e) {
            mLastError = mLastError+" "+e.toString();
            Const.Toast(context, mLastError);
            disconnect();
            return false;
        }
        return true;
    }

    public void disconnect(){
        try {
        if(in!=null){
            in.close();
        }
        if(out!=null){
            out.close();
        }
        if(mSocket!=null){
            mSocket.close();
        }
        }
        catch (IOException e) {
            e.printStackTrace();
            Const.Toast(context, e.getMessage());
        }

    }

    public void send(String message){
        send(message, Const.WIN1251);
    }

    public void send(String message, String charsetName){
        try {
            out.write(message.getBytes(charsetName));
        } catch (IOException e) {
            mLastError = e.toString();
            Const.Toast(context, mLastError);
            disconnect();
        }
    }

    public int getFileSize(){
        try {
            return mSocket.getReceiveBufferSize();
        } catch (SocketException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String receive(){
        StringBuilder res = new StringBuilder();
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while((line = br.readLine())!=null){
                int pos = line.indexOf(Const.END_MESSAGE);
                if(pos!=-1){
                    line = line.substring(0, pos);
                }
                res.append(line);
                Const.Toast(context, res.toString());
            }
        }
        catch (Exception e){
            mLastError = e.toString();
            Const.Toast(context, mLastError);
            return mLastError;
        }
        return res.toString();
    }

    public boolean receiveFile(String filePath){
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePath, false);
            byte[] buf = new byte[1000];
            int count = in.read(buf,0,1000);
            String message = new String(buf, Const.UTF8);
            if(message.contains(ERROR)){
                int pos = message.indexOf(ERROR);
                if(pos>1){
                    mLastError = message.substring(0, pos-1);
                    Const.Toast(context, mLastError);
                }
                return false;
            }
            else{
                fos.write(buf, 0, count);
            }

            while((count = in.read(buf))>0){
                fos.write(buf, 0, count);
            }
            return true;
        } catch (Exception e) {
            mLastError = e.toString();
            Const.Toast(context, mLastError);
        } finally {
            try{
                fos.close();
            }
            catch (Exception e2){
                mLastError += e2.toString();
                Const.Toast(context, mLastError);
            }
        }
        return false;
    }

    public void flushOutput(){
        if(out!=null){
            try{
                out.flush();
            }
            catch (Exception e){
                mLastError = e.toString();
                Const.Toast(context, mLastError);
            }
        }
    }

    public String LastError(){
        return mLastError;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
