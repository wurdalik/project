package ru.dalimo.akimov.mylearning.ifaces;

import ru.dalimo.akimov.mylearning.asynctasks.GetFilesFromServerTask;
import ru.dalimo.akimov.mylearning.asynctasks.UnpackFilesTask;
import ru.dalimo.akimov.mylearning.asynctasks.WriteToDatabaseTask;

public interface IGetFilesComplete {
    void onTaskComplete(GetFilesFromServerTask task);
    void onTaskComplete(UnpackFilesTask task);
    void onTaskComplete(WriteToDatabaseTask task);
}
