package ru.dalimo.akimov.mylearning.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.File;
import java.util.concurrent.ExecutionException;

import ru.dalimo.akimov.mylearning.Const;
import ru.dalimo.akimov.mylearning.R;
import ru.dalimo.akimov.mylearning.asynctasks.GetFilesFromServerTask;
import ru.dalimo.akimov.mylearning.asynctasks.UnpackFilesTask;
import ru.dalimo.akimov.mylearning.asynctasks.WriteToDatabaseTask;
import ru.dalimo.akimov.mylearning.connections.MMerchClient;
import ru.dalimo.akimov.mylearning.connections.UserSettings;
import ru.dalimo.akimov.mylearning.ifaces.IGetFilesComplete;

public class MainFragment extends Fragment implements IGetFilesComplete {

    private UserSettings userSettings;
    private GetFilesFromServerTask update;
    private UnpackFilesTask unpack;
    private WriteToDatabaseTask writeToDatabaseTask;

    private ListClientsFragment listClientsFragment;

    private File f;

    private MMerchClient mClient;
    private Button new_visit;

    public static MainFragment newInstance(UserSettings userSettings){
        Bundle args = new Bundle();
        args.putParcelable(UserSettings.KEY, userSettings);
        MainFragment mainFragment = new MainFragment();
        mainFragment.setArguments(args);
        return mainFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        userSettings = getArguments().getParcelable(UserSettings.KEY);
        mClient = new MMerchClient(getActivity(), userSettings);
    }

    @Override
    public void onResume() {
        super.onResume();
        userSettings = Const.loadSettings(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.main_fragment, null);
        new_visit = v.findViewById(R.id.new_visit_btn);
        new_visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listClientsFragment = ListClientsFragment.newInstatce(userSettings.getCodeAgent());
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, listClientsFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        menu.clear();
        inflater.inflate(R.menu.menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case (R.id.update_files):
                updateFiles();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateFiles(){
        update = new GetFilesFromServerTask(getActivity(), userSettings, MainFragment.this);
        update.execute(Const.ARCHIVE_NAME);
    }

    @Override
    public void onTaskComplete(GetFilesFromServerTask task) {
        boolean res;
        try {
            res=task.get();
            if(res){
                Const.Toast(getActivity(), "Файл загружен");
                unpack = new UnpackFilesTask(getActivity(), MainFragment.this);
                unpack.execute(Const.prepareWorkPath(), Const.ARCHIVE_NAME + Const.ARCH_EXT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(UnpackFilesTask task) {
        boolean res;
        try {
            res = task.get();
            if(res){
                writeToDatabaseTask = new WriteToDatabaseTask(getActivity() , userSettings.getCodeAgent(), MainFragment.this);
                writeToDatabaseTask.execute(Const.PATH+File.separator+Const.END_FOLDER);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskComplete(WriteToDatabaseTask task) {
        boolean res;
        try {
            res = task.get();
            if(res){
                Const.Toast(getActivity(), "Записано в БД");
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
