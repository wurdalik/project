package ru.dalimo.akimov.mylearning.asynctasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import ru.dalimo.akimov.mylearning.Client;
import ru.dalimo.akimov.mylearning.Const;
import ru.dalimo.akimov.mylearning.Manager;
import ru.dalimo.akimov.mylearning.ifaces.IGetFilesComplete;
import ru.dalimo.akimov.mylearning.database.ProjectLab;

public class WriteToDatabaseTask extends AsyncTask<String, Integer, Boolean>{
    private final static String TAG = "#COUNT->";

    private ProgressDialog progress;

    private String userCode;
    private Activity context;

    private IGetFilesComplete listener;


    public WriteToDatabaseTask(Activity context, String userCode, IGetFilesComplete listener){
        this.context = context;
        this.userCode = userCode;
        this.listener = listener;
        progress = new ProgressDialog(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress.setMessage("Запись в базу данных");
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        writeFilesToBase(context, strings[0]);
        return true;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    private void writeFilesToBase(Activity context, String filePath){
        String[] files = new File(filePath).list();

        for(String filename : files){
            File file = new File(filePath,filename);
            if(!file.exists()){
               Const.Toast(context, "Файл не существует" + filename);
                continue;
            }
            switch (filename){
                case Const.KLIENTS:
                    parceClients(context, file);
                    break;
                case Const.MANAGERS:
                    parceManag(context, file);
                    break;
                case Const.MATRIX:
                    parceMatrix(context, file);
                    break;
                case Const.OSTATKI:
                    parceGoods(context, file);
                    break;
//                case Const.SMTP:
////                    parseSMTP();
//                    break;
            }
        }
    }

    private void parceClients(Context context, File f) {
        BufferedReader br;
        String line;

        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "Cp1251"));
            List<String[]> clients = new ArrayList<>();
            while((line = br.readLine())!=null && !line.equalsIgnoreCase(" ")){
                String userCode = "";
                String clientCode = "";
                String clientTitle = "";
                String clientInn = "";
                String clientAddress = "";
                String clientNumber = "";
                String clientDirector = "";

                String[] arr = line.split(";");
                int i = 0;
                for(String clientInfo:arr){
                    switch (i){
                        case 0:
                            userCode = clientInfo;
                            break;
                        case 1:
                            clientCode = clientInfo;
                            break;
                        case 2:
                            clientTitle = clientInfo;
                            break;
                        case 3:
                            clientInn = clientInfo;
                            break;
                        case 4:
                            clientAddress = clientInfo;
                            break;
                        case 5:
                            clientNumber = clientInfo;
                            break;
                        case 6:
                            clientDirector = clientInfo;
                            break;
                    }
                    i++;
                }
                clients.add(new String[]{userCode, clientCode, clientTitle, clientInn, clientAddress, clientNumber, clientDirector});
            }
            ProjectLab.get(context).addClient(clients);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parceManag(Context context, File f){
        BufferedReader br;

        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "Cp1251"));
            List<String[]>managers = new ArrayList<>();
            String line;
            while((line = br.readLine())!=null && !line.equalsIgnoreCase(" ")){
                String userCode = "";
                String clientCode = "";
                String managerCode = "";
                String managerName = "";

                String[] arr = line.split(";");
                int i = 0;
                for(String managerInfo:arr){
                    switch (i){
                        case 0:
                            userCode = managerInfo;
                            break;
                        case 1:
                            clientCode = managerInfo;
                            break;
                        case 2:
                            managerCode = managerInfo;
                            break;
                        case 3:
                            managerName = managerInfo;
                            break;
                    }
                    i++;
                }
                managers.add(new String[]{userCode, clientCode, managerCode, managerName});
            }
            ProjectLab.get(context).addManager(managers);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parceMatrix(Context context, File f){
        BufferedReader br;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "Cp1251"));
            List<String[]>matrix = new ArrayList<>();
            String line;
            while((line = br.readLine())!=null && !line.equalsIgnoreCase(" ")){
                String userCode = "";
                String clientCode = "";
                String productCode = "";
                String exclusive = "";

                String[] arr = line.split(";");
                int i = 0;
                for(String matrixInfo:arr){
                    switch (i){
                        case 0:
                            userCode = matrixInfo;
                            break;
                        case 1:
                            clientCode = matrixInfo;
                            break;
                        case 2:
                            productCode = matrixInfo;
                            break;
                        case 3:
                            exclusive = matrixInfo;
                            break;
                    }
                    i++;
                }
                matrix.add(new String[]{userCode, clientCode, productCode, exclusive});
            }
            ProjectLab.get(context).writeMatrix(matrix);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "parceMatrix: " +e.toString() );
        }
    }

    private void parceGoods(Activity context, File f){
        BufferedReader br;
        try{
            br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "Cp1251"));
            List<String[]>goods = new ArrayList<>();
            String line;
            while((line=br.readLine())!=null && !line.equalsIgnoreCase(" ")){
                String productType = "";
                String parentCode = "";
                String productCode = "";
                String productName = "";

                String[] arr = line.split(";");
                int i = 0;
                for(String goodInfo:arr){
                    switch (i){
                        case 0:
                            productType = goodInfo;
                            break;
                        case 1:
                            parentCode = goodInfo;
                            break;
                        case 2:
                            productCode = goodInfo;
                            break;
                        case 3:
                            productName = goodInfo;
                            break;
                    }
                    i++;
                }
                goods.add(new String[]{productType, parentCode, productCode, productName});
            }
            ProjectLab.get(context).writeGoods(goods);
        }
        catch (Exception e){
            e.printStackTrace();
            Const.Toast(context, e.toString());
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if(listener!=null){
            listener.onTaskComplete(this);
        }
        progress.dismiss();
    }
}
