package ru.dalimo.akimov.mylearning.ifaces;

import android.location.Location;

import ru.dalimo.akimov.mylearning.asynctasks.GetLocationTask;

public interface IGetLocationComplete {
    void onGetLocationComplete(GetLocationTask task, Location location);
}
