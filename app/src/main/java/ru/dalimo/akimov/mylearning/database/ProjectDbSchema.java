package ru.dalimo.akimov.mylearning.database;

public class ProjectDbSchema {
    public static final class ClientsTable{
        public static final String NAME = "clients";
        public static class Cols{
            public static final String USER_CODE = "userCode";
            public static final String CLIENT_CODE = "clientCode";
            public static final String TITLE = "clientTitle";
            public static final String INN = "clientINN";
            public static final String ADDRESS = "clientAddress";
            public static final String NUMBER = "clientNumber";
            public static final String DIRECTOR = "clientDirector";
        }
    }

    public static final class ManagersTable{
        public static final String NAME = "managers";
        public static class Cols{
            public static final String USER_CODE = "userCode";
            public static final String CLIENT_CODE = "clientCode";
            public static final String MANAGER_CODE = "managerCode";
            public static final String MANAGER_NAME = "managerName";
        }
    }

    public static final class Matrix{
        public static final String NAME = "matrix";
        public static class Cols{
            public static final String USER_CODE = "userCode";
            public static final String CLIENT_CODE = "clientCode";
            public static final String PRODUCT_CODE = "productCode";
            public static final String EXCLUSIVE = "exclusive";
        }
    }

    public static final class GoodsMetaData{
        public static final String NAME = "GoodsInStore";
        public static class Cols{
            public static final String PRODUCT_TYPE = "productType";
            public static final String PARENT_CODE = "parentCode";
            public static final String PRODUCT_CODE = "productCode";
            public static final String PRODUCT_NAME = "productName";
        }
    }



}
