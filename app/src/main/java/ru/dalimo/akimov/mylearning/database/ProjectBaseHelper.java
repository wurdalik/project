package ru.dalimo.akimov.mylearning.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ProjectBaseHelper extends SQLiteOpenHelper{
    private static final int VERSION =1;
    private static final String DATABASE_NAME = "projectBase.db";

    public ProjectBaseHelper(Context context){
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + ProjectDbSchema.ClientsTable.NAME +
        "(" +   ProjectDbSchema.ClientsTable.Cols.USER_CODE +" text, " +
                ProjectDbSchema.ClientsTable.Cols.CLIENT_CODE +" text, " +
                ProjectDbSchema.ClientsTable.Cols.TITLE + " text, " +
                ProjectDbSchema.ClientsTable.Cols.INN + " text, " +
                ProjectDbSchema.ClientsTable.Cols.ADDRESS + " text, " +
                ProjectDbSchema.ClientsTable.Cols.NUMBER + " text, " +
                ProjectDbSchema.ClientsTable.Cols.DIRECTOR + " text);");


        sqLiteDatabase.execSQL("create table " + ProjectDbSchema.ManagersTable.NAME +
        "(" +   ProjectDbSchema.ManagersTable.Cols.USER_CODE +" text, " +
                ProjectDbSchema.ManagersTable.Cols.CLIENT_CODE +" text, " +
                ProjectDbSchema.ManagersTable.Cols.MANAGER_CODE +" text, " +
                ProjectDbSchema.ManagersTable.Cols.MANAGER_NAME +" text);");

        sqLiteDatabase.execSQL("create table " + ProjectDbSchema.Matrix.NAME +
        "(" +   ProjectDbSchema.Matrix.Cols.USER_CODE + " text, " +
                ProjectDbSchema.Matrix.Cols.CLIENT_CODE + " text, " +
                ProjectDbSchema.Matrix.Cols.PRODUCT_CODE + " text, " +
                ProjectDbSchema.Matrix.Cols.EXCLUSIVE +" text);");

        sqLiteDatabase.execSQL("create table " + ProjectDbSchema.GoodsMetaData.NAME +
        "(" +
                ProjectDbSchema.GoodsMetaData.Cols.PRODUCT_TYPE+" text, " +
                ProjectDbSchema.GoodsMetaData.Cols.PARENT_CODE+" text, " +
                ProjectDbSchema.GoodsMetaData.Cols.PRODUCT_CODE+" text, " +
                ProjectDbSchema.GoodsMetaData.Cols.PRODUCT_NAME+ " text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
