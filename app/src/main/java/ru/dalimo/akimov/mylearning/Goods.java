package ru.dalimo.akimov.mylearning;

import android.os.Parcel;
import android.os.Parcelable;

public class Goods implements Parcelable {
    public static final String GOODS = "goods";
    private String goodType;
    private String parentCode;
    private String goodCode;
    private String productName;

    public Goods(String goodType, String parentCode, String goodCode, String productName){
        this.goodType = goodType;
        this.parentCode = parentCode;
        this.goodCode = goodCode;
        this.productName = productName;
    }

    public String getGoodType() {
        return goodType;
    }

    public void setGoodType(String goodType) {
        this.goodType = goodType;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getGoodCode() {
        return goodCode;
    }

    public void setGoodCode(String goodCode) {
        this.goodCode = goodCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(goodType);
        dest.writeString(parentCode);
        dest.writeString(goodCode);
        dest.writeString(productName);
    }

    public static final Parcelable.Creator<Goods> CREATOR = new Parcelable.Creator<Goods>(){

        @Override
        public Goods createFromParcel(Parcel source) {
            return new Goods(source);
        }

        @Override
        public Goods[] newArray(int size) {
            return new Goods[size];
        }
    };

    private Goods(Parcel in){
        goodType = in.readString();
        parentCode = in.readString();
        goodCode = in.readString();
        productName = in.readString();
    }
}
