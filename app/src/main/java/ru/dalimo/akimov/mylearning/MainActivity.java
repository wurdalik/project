package ru.dalimo.akimov.mylearning;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.util.ArrayList;

import ru.dalimo.akimov.mylearning.connections.UserSettings;
import ru.dalimo.akimov.mylearning.fragments.MainFragment;
import ru.dalimo.akimov.mylearning.fragments.ListClientsFragment;
import ru.dalimo.akimov.mylearning.fragments.SettingsFragment;

public class MainActivity extends AppCompatActivity{
    private static final int PERMISSIONS = 1;
    private UserSettings userSettings;
    private String userCode;
    private String server;
    private int port;

    private MainFragment mainFragment;



    SharedPreferences sp;
    private Toolbar toolbar;
    private MainFragment mFragment;
    private SettingsFragment settingsFragment;
    private FragmentTransaction transaction;

    private DrawerLayout mDrawerLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userSettings = Const.loadSettings(this);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar_activity);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case (R.id.new_monitoring):
                        ListClientsFragment listClientsFragment = new ListClientsFragment();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragment_container, listClientsFragment)
                                        .addToBackStack(null)
                                        .commit();
                        break;
                    case (R.id.update_files):
                        Permissions.check(MainActivity.this, Const.PERMISSIONS, Const.rationale, Const.options, new PermissionHandler() {
                            @Override
                            public void onGranted() {
                                Const.prepareWorkPath();
                                mFragment.updateFiles();
                            }

                            @Override
                            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                                Const.Toast(MainActivity.this, "В доступе отказано");
                            }
                        });

                        break;
                    case (R.id.update_app):
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.madlab.mmerch.shikhevich.vladislav"));
                        startActivity(intent);
                        break;
                    case (R.id.settings):
                        settingsFragment = new SettingsFragment();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragment_container, settingsFragment)
                                        .addToBackStack(null)
                                        .commit();
                        break;
                }
                mDrawerLayout.closeDrawers();
                return true;
            }
        });

        mFragment = MainFragment.newInstance(userSettings);
        FragmentManager ft = getSupportFragmentManager();
        ft.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        ft.beginTransaction()
                .replace(R.id.fragment_container, mFragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case (android.R.id.home):
                mDrawerLayout.openDrawer(Gravity.START);
                return true;
        }
        return false;
    }

}
