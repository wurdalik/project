package ru.dalimo.akimov.mylearning.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ru.dalimo.akimov.mylearning.GoodsItem;
import ru.dalimo.akimov.mylearning.R;
import ru.dalimo.akimov.mylearning.adapters.GoodsItemAdapter;

public class ListGoodsFragment extends Fragment {
    private RecyclerView recyclerView;
    private ArrayList<GoodsItem> goodsItems;
    private GoodsItemAdapter adapter;

    public static ListGoodsFragment newInstance(ArrayList<GoodsItem>goodsItem){
        ListGoodsFragment listGoodsFragment = new ListGoodsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("list", goodsItem);
        listGoodsFragment.setArguments(args);
        return listGoodsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goodsItems = getArguments().getParcelableArrayList("list");
        adapter = new GoodsItemAdapter(getActivity(), goodsItems);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_goods_item_recycle, container, false);
        recyclerView = v.findViewById(R.id.goods_item_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        return v;
    }

}
