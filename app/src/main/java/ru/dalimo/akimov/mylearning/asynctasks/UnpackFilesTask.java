package ru.dalimo.akimov.mylearning.asynctasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import ru.dalimo.akimov.mylearning.Const;
import ru.dalimo.akimov.mylearning.ifaces.IGetFilesComplete;

public class UnpackFilesTask extends AsyncTask<String, String, Boolean> {
    private Context baseContext;
    private File f;

    private ProgressDialog progress;

    private IGetFilesComplete listener;

    public UnpackFilesTask(Activity context, IGetFilesComplete listener){
        baseContext = context;
        this.listener = listener;
        progress = new ProgressDialog(context);
        progress.setMessage("Распаковка архива");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    }

    @Override
    protected void onPreExecute() {
        progress.show();

    }

    @Override
    protected Boolean doInBackground(String... strings) {
        File unpackFold = new File(strings[0], Const.END_FOLDER);
        if(!unpackFold.exists()){unpackFold.mkdirs();}
        f = new File(strings[0], strings[1]);

        if (f.exists()){
        for(String files : strings){
            unzip(f, unpackFold.getPath());
        }
        return true;
    }else{
            return false;
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);

        progress.setMessage("Распаковка файла: " + values[0]);
//        progress.setProgress(
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
       if(listener!=null)
        listener.onTaskComplete(this);

       progress.dismiss();
    }

    public void unzip(File zipFile, String filePath) {
        ZipInputStream zis = null;
        try {
            zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)));
            ZipEntry ze;
            int count;

            while((ze = zis.getNextEntry())!=null) {
                byte[] buffer = new byte[1024];
                publishProgress(ze.getName());
                File file = new File(filePath, ze.getName());
                FileOutputStream fout = new FileOutputStream(file);
                while ((count = zis.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }
                fout.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
