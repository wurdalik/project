package ru.dalimo.akimov.mylearning;

public class Manager {
    private String codeManag;
    private String nameManag;

    public Manager(String codeManag, String nameManag){
        this.codeManag = codeManag;
        this.nameManag = nameManag;
    }

    public String getCodeManag() {
        return codeManag;
    }

    public void setCodeManag(String codeManag) {
        this.codeManag = codeManag;
    }

    public String getNameManag() {
        return nameManag;
    }

    public void setNameManag(String nameManag) {
        this.nameManag = nameManag;
    }
}
